Summary
=======
This is the DuMuX module containing the code for producing the results of

* Riethmüller (2020): *A comparative study of the monolithic and the partitioned preCICE based coupling approaches
for the solution of coupled Stokes-Darcy problems*

,

* Riethmüller (2021): *Investigation of linear solvers and preconditioners for sparse systems resulting from free-flow applications*

and

* Schmalfuß, Riethmüller, Altenbernd, Weishaupt, Göddeke (2021): *Partitioned Coupling vs Monolithic Block-Preconditioning Approaches for Solving Stokes-Darcy Systems*

The code is hosted in the branches _cedric_, _thesis_ and _paper_ respectively.
